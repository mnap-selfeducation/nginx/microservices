# Microservices March 2022

NGINX's course on Kubernetes. All examples are available at the [NGINX GitHub repo](https://github.com/nginxinc/microservices-march).

Check running pods, services, deployments and nodes at any time with:
```shell
kubectl get pods
kubectl get services
kubectl get deployments
kubectl get nodes

minikube service list
minikube node list
```

Run service in a browser with:
```shell
minikube service <service-name>
```

## Lab 1: Reduce Kubernetes Latency with Autoscaling

[NGINX Tutorial: Reduce Kubernetes Latency with Autoscaling](https://www.nginx.com/blog/microservices-march-reduce-kubernetes-latency-with-autoscaling/)

Use NGINX Ingress Controller to expose an app and then autoscale the ingress controller pods in response to high traffic.


### 1. Configure a simple app on a Kubernetes cluster

```shell
minikube start

kubectl apply -f 1-deployment.yaml

minikube service podinfo

kubectl get pods

minikube service podinfo
```

### 2. Use NGINX Ingress Controller to expose the app

```shell
helm repo add nginx-stable https://helm.nginx.com/stable

helm install main nginx-stable/nginx-ingress \
  --set controller.watchIngressWithoutClass=true \
  --set controller.service.type=NodePort \
  --set controller.service.httpPort.nodePort=30005

kubectl get pods

kubectl apply -f 2-ingress.yaml
```

### 3. Visualize and generate traffic

```shell
kubectl get pods -o wide
kubectl run -ti --rm=true busybox --image=busybox
/# wget -qO- <IP_address>:9113/metrics
/# exit

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm install prometheus prometheus-community/prometheus \
  --set server.service.type=NodePort --set server.service.nodePort=30010

kubectl get pods

minikube service prometheus-server

kubectl apply -f 3-locust.yaml

minikube service locust
```

Locust load test parameters:

- Number of users - 1000
- Spawn rate - 10
- Host - http://main-nginx-ingress

### 4. Autoscale NGINX Ingress Controller

```shell
helm repo add kedacore https://kedacore.github.io/charts

helm install keda kedacore/keda

kubectl get pods

kubectl apply -f 4-scaled-object.yaml
```

Locust load test parameters:

- Number of users - 2000
- Spawn rate - 10
- Host - http://main-nginx-ingress

```shell
kubectl get hpa
```

## Lab2: Protect Kubernetes APIs with Rate Limiting

[NGINX Tutorial: Protect Kubernetes APIs with Rate Limiting](https://www.nginx.com/blog/microservices-march-protect-kubernetes-apis-with-rate-limiting/)

Use multiple NGINX Ingress Controllers combined with rate limiting to prevent apps and APIs from getting overwhelmed.

### 1. Deploy a Cluster, App, API, and Ingress Controller

```shell
minikube start

kubectl apply -f 1-apps.yaml

kubectl get pods

helm install main nginx-stable/nginx-ingress \ 
  --set controller.watchIngressWithoutClass=true \ 
  --set controller.ingressClass=nginx \ 
  --set controller.service.type=NodePort \ 
  --set controller.service.httpPort.nodePort=30010 \ 
  --set controller.enablePreviewPolicies=true \ 
  --namespace nginx 

kubectl get pods --namespace nginx

kubectl apply -f 2-ingress.yaml

kubectl run -ti --rm=true busybox --image=busybox
/# wget --header="Host: api.usrv.test" -qO- main-nginx-ingress.nginx
/# wget --header="Host: usrv.test" --header="User-Agent: Mozilla" -qO- main-nginx-ingress.nginx
/# exit

minikube service frontend
```

### 2. Overwhelm Your App and API

```shell
kubectl apply -f 3-locust.yaml

kubectl get pods

minikube service locust
```

Locust load test parameters:

- Number of users - 1000
- Spawn rate - 30
- Host - http://main-nginx-ingress

### 3. Save Your App and API with Dual Ingress Controller and Rate Limiting

```shell
kubectl delete -f 2-ingress.yaml

kubectl create namespace nginx-web
kubectl create namespace nginx-api

helm install web nginx-stable/nginx-ingress \
  --set controller.ingressClass=nginx-web \ 
  --set controller.service.type=NodePort \ 
  --set controller.service.httpPort.nodePort=30020 \ 
  --namespace nginx-web

kubectl apply -f 4-ingress-web.yaml

helm install api nginx-stable/nginx-ingress \ 
  --set controller.ingressClass=nginx-api \ 
  --set controller.service.type=NodePort \ 
  --set controller.service.httpPort.nodePort=30030 \ 
  --set controller.enablePreviewPolicies=true \ 
  --namespace nginx-api

kubectl apply -f 5-ingress-api.yaml

kubectl apply -f 6-locust.yaml

kubectl delete pod $(kubectl get pods | grep locust | awk {'print $1'})

kubectl get pods -A
```

Locust load test parameters:

- Number of users - 400
- Spawn rate - 10
- Host - http://main-nginx-ingress

## Lab 3: Protect Kubernetes Apps from SQL Injection

[NGINX Tutorial: Protect Kubernetes Apps from SQL Injection](https://www.nginx.com/blog/microservices-march-protect-kubernetes-apps-from-sql-injection/)

Use NGINX and NGINX Ingress Controller to block SQL injection.

### 1. Deploy a Cluster and Vulnerable App

```shell
minikube start

kubectl apply -f 1-app.yaml

kubectl get pods
```

### 2. Hack the App

```
http://app.url:30001/product/-1

http://app.url:30001/product/-1" UNION SELECT * FROM users -- //"

http://app.url:30001/product/-1" UNION SELECT password FROM users; -- // <-- 1 column 
http://app.url:30001/product/-1" UNION SELECT password,password FROM users; -- // <-- 2 columns 
http://app.url:30001/product/-1" UNION SELECT password,password,password FROM users; -- // <-- 3 columns 
http://app.url:30001/product/-1" UNION SELECT password,password,password,password FROM users; -- // <-- 4 columns 
http://app.url:30001/product/-1" UNION SELECT password,password,password,password,password FROM users; -- // <-- 5 columns"

http://app.url:30001/product/-1" UNION SELECT username,username,password,password,username FROM users where id=1 -- //"
```

### 3. Use an NGINX Sidecar Container to Block Certain Requests

```shell
kubectl apply -f 2-app-sidecar.yaml
```

### 4. Configure NGINX Ingress Controller to Filter Requests

```shell
helm repo add nginx-stable https://helm.nginx.com/stable

helm install main nginx-stable/nginx-ingress \
    --set controller.watchIngressWithoutClass=true \
    --set controller.service.type=NodePort \
    --set controller.service.httpPort.nodePort=30005 \
    --set controller.enableSnippets=true

kubectl get pods

kubectl delete -f 2-app-sidecar.yaml
kubectl apply -f 1-app.yaml
kubectl apply -f 3-ingress.yaml

kubectl run -ti --rm=true busybox --image=busybox 
/# wget --header="Host: usrv.test" -qO- main-nginx-ingress
/# wget --header="Host: usrv.test" -qO- 'main-nginx-ingress/product/-1"%20UNION%20SELECT%20username,username,password,password,username%20FROM%20users%20where%20id=1%20--%20//'
/# exit
```

## Lab 4: Improve Uptime and Resilience with a Canary Deployment

[NGINX Tutorial: Improve Uptime and Resilience with a Canary Deployment](https://www.nginx.com/blog/microservices-march-improve-kubernetes-uptime-and-resilience-with-a-canary-deployment/)

Implement a gradual, controlled migration using the traffic splitting technique “canary deployment” because it provides a safe and agile way to test the stability of a new feature or version. Your use case involves traffic moving between two Kubernetes services.

### 1. Deploy a Cluster and NGINX Service Mesh

```shell
minikube start \ 
    --extra-config=apiserver.service-account-signing-key-file=/var/lib/minikube/certs/sa.key \ 
  --extra-config=apiserver.service-account-key-file=/var/lib/minikube/certs/sa.pub \ 
  --extra-config=apiserver.service-account-issuer=kubernetes/serviceaccount \ 
  --extra-config=apiserver.service-account-api-audiences=api

helm install nsm nginx-stable/nginx-service-mesh  --namespace nginx-mesh --create-namespace

kubectl get pods --namespace nginx-mesh
```

### 2. Deploy Two Apps (a Frontend and a Backend)

```shell
kubectl apply -f 1-backend-v1.yaml

kubectl get pods,services

kubectl apply -f 2-frontend.yaml

kubectl get pods

kubeclt logs -c frontend frontend-<pod id>

minikube service jaeger
# or
kubectl --namespace nginx-mesh expose pod jaeger-<pod id> --type=NodePort

kubectl apply -f 3-backend-v2.yaml

kubeclt logs -c frontend frontend-<pod id>
```

### 3. Use NGINX Service Mesh to Implement a Canary Deployment

```shell
kubectl apply -f 4-services.yaml

kubeclt logs -c frontend frontend-<pod id>

kubectl apply -f 5-split.yaml

kubeclt logs -c frontend frontend-<pod id>
```

## Extras: Deployments using Traffic Splitting

[Deployments using Traffic Splitting](https://mesh-public-docs.netlify.app/nginx-service-mesh/tutorials/trafficsplit-deployments/)

A guide for using traffic splits with different deployment strategies.

### Deploy the Production Version of the Target App

```shell
minikube start \ 
    --extra-config=apiserver.service-account-signing-key-file=/var/lib/minikube/certs/sa.key \ 
  --extra-config=apiserver.service-account-key-file=/var/lib/minikube/certs/sa.pub \ 
  --extra-config=apiserver.service-account-issuer=kubernetes/serviceaccount \ 
  --extra-config=apiserver.service-account-api-audiences=api

helm install nsm nginx-stable/nginx-service-mesh  --namespace nginx-mesh --create-namespace

kubectl get pods --namespace nginx-mesh
```

Generate traffic to `gateway-svc`:
```shell
kubectl get svc gateway-svc

export GATEWAY_IP=<gateway external IP>

for i in $(seq 1 300); do curl $GATEWAY_IP; sleep 1; done
```

If on Minikube open tunnel and check external IP this way:
```shell
minikube tunnel

minikube service --all
```

### Deploy a New Version of the Target App using a Canary Deployment

```shell
kubectl apply -f trafficsplit.yaml

kubectl get ts

kubectl describe ts target-ts

kubectl apply -f target-v2.0-failing.yaml

kubectl get pods
kubectl get svc
```

Change the weights of targets in the `trafficsplit.yaml`, e.g. to 90/10
```shell
kubect apply -f trafficsplit.yaml
```

Since target 2.0 is failing, change back the weights or delete the failing target
```shell
kubectl delete -f target-v2.0-failing.yaml
```

### Deploy a New Version of the Target App using a Blue-Green Deployment

```shell
kubectl apply -f target-v2.1-successful.yaml
```

Change the weights of targets in the `trafficsplit.yaml`, to 0/100
```shell
kubectl apply -f trafficsplit.yaml
```

Since target 2.1 is working as expected, delete previous version
```shell
kubectl delete -f target-v1.0.yaml
```

### A/B Testing with Traffic Splits

```shell
kubectl apply -f target-v3.0.yaml

kubectl apply -f trafficsplit-matches.yaml

kubectl get httproutegroups<tab> 
kubectl get trafficsplits<tab>

kubectl describe httproutegroups<tab> 
kubectl describe trafficsplits<tab>

export GATEWAY_IP=<gateway external IP>

for i in $(seq 1 300); do curl $GATEWAY_IP; sleep 1; done

for i in $(seq 1 100); do curl $GATEWAY_IP -H "user-agent:Firefox"; sleep 1; done
```

### Traffic Splitting based on path and HTTP methods

```shell
kubectl edit httproutegroup target-hrg
```

or edit `trafficsplit-matches`
```conf
spec:
  matches:
  - name: firefox-users
    headers:
      - user-agent: ".*Firefox.*"
  - name: get-api-requests
    pathRegex: "/api"
    methods:
      - GET
```
and
```shell
kubectl apply -f trafficsplit-matches.yaml

for i in $(seq 1 100); do curl $GATEWAY_IP; sleep 1; done
for i in $(seq 1 100); do curl $GATEWAY_IP/api; sleep 1; done
for i in $(seq 1 100); do curl $GATEWAY_IP -H "user-agent:Firefox"; sleep 1; done
```

## Extras: Deploy an Example App with NGINX Service Mesh

[Deploy an Example App with NGINX Service Mesh](https://docs.nginx.com/nginx-service-mesh/tutorials/deploy-example-app/)

A walkthrough of deploying an App with NGINX Service Mesh.

```shell
minikube start \ 
    --extra-config=apiserver.service-account-signing-key-file=/var/lib/minikube/certs/sa.key \ 
  --extra-config=apiserver.service-account-key-file=/var/lib/minikube/certs/sa.pub \ 
  --extra-config=apiserver.service-account-issuer=kubernetes/serviceaccount \ 
  --extra-config=apiserver.service-account-api-audiences=api

helm install nsm nginx-stable/nginx-service-mesh  --namespace nginx-mesh --create-namespace

kubectl get pods --namespace nginx-mesh
```

### Inject the Sidecar Proxy

Automatically:
```shell
kubectl apply -f bookinfo.yaml
```

Manually:
```shell
nginx-meshctl inject < examples/bookinfo.yaml | kubectl apply -f -
```

### Verify that the Sample App Works Correctly

```shell
kubectl port-forward svc/productpage 9080
```

Open the Service URL in a browser: `http://localhost:9080`.

### Verify NGINX Service Mesh Features

Check the Grafana dashboard
```shell
kubectl -n nginx-mesh port-forward grafana-<grafana-id> 3000
```
Open the Grafana URL in a browser: `http://localhost:3000`.

Check Prometheus metrics 
```shell
kubectl -n nginx-mesh port-forward prometheus-<prometheus-id> 9090
```
Open the Prometheus server URL in a browser window: `http://localhost:9090/graph`.

Check Tracing
```shell
kubectl -n nginx-mesh port-forward jaeger-<jaeger-id> 16686
```
Open the tracing server URL in a browser: http://localhost:16686.

### Extras: Monitoring your NGINX Plus-based Ingress controller in Kubernetes

[Monitoring your NGINX Plus-based Ingress controller in Kubernetes](https://github.com/jasonwilliams14/nginx-ingress-visibility)

This setup is going to walk you through how to install several tools to monitor your NGINX Ingress controller in your Kubernetes cluster.
This tutorial is part of a [How to Improve Visibility in Kubernetes with Prometheus, Grafana, and NGINX](https://www.youtube.com/watch?v=hJoH7J0un5U) video. For Grafana Loki dashboard search Grafana Labs [dashboards](https://grafana.com/grafana/dashboards/12559).

```shell
minikube start \ 
    --extra-config=apiserver.service-account-signing-key-file=/var/lib/minikube/certs/sa.key \ 
  --extra-config=apiserver.service-account-key-file=/var/lib/minikube/certs/sa.pub \ 
  --extra-config=apiserver.service-account-issuer=kubernetes/serviceaccount \ 
  --extra-config=apiserver.service-account-api-audiences=api
```

Setup Prometheus

```shell
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add kube-state-metrics https://kubernetes.github.io/kube-state-metrics
helm repo update

helm install prometheus-community/prometheus --generate-name

kubectl get pods -A
helm ls -A

kubectl --namespace default port-forward prometheus-<server-id> 9090
kubectl --namespace default port-forward prometheus-<pushgateway-id> 9091
```

Setup Grafana

```shell
helm repo add grafana https://grafana.github.io/helm-charts

helm install nginx-logs grafana/grafana

kubectl get pods -A
helm ls -A

kubectl --namespace default port-forward nginx-logs-grafana-<grafana-id> 3000
```

Setup NGINX (open source NGINX or NGINX Plus)

```shell
kubectl create namespace nginx-ingress
```

via Helm Repository
```shell
helm install nginx-test nginx-stable/nginx-ingress
helm install nginx-test nginx-stable/nginx-ingress --set controller.image.repository=myregistry.example.com/nginx-plus-ingress --set controller.nginxplus=true
```

using Chart Sources
```shell
helm install nginx-test .
helm install nginx-test -f values-plus.yaml .
```

```shell
kubectl apply -f virtual-server.yaml
kubectl apply -f virtual-server-route.yaml
```
